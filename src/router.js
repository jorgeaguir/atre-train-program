import VueRouter from 'vue-router';
import List from './components/List.vue';
import Exe from './components/Train.vue';
import Train from './components/Exe.vue';
import Add from './components/Add.vue';
import addEjercicio from './components/add-components/ejercicio.vue';
import addRutina from './components/add-components/rutina.vue';

const routes = [
    {
        name: 'home',
        path: '/',
        component: List
    },
    {
        name: 'trains',
        path: '/trains',
        component: Train
    },
    {
        name: 'exe',
        path: '/ejercicios/:exe',
        component: Exe
    },
    {
        name: 'add',
        path: '/add',
        component: Add
    },
    {
        name: 'addEjercicio',
        path: '/add/ejercicios',
        component: addEjercicio
    },
    {
        name: 'addRutina',
        path: '/add/rutinas',
        component: addRutina
    },
    {
        path: '*',
        redirect: '/'
    }
];

const router = new VueRouter({
    routes
});

export default router;